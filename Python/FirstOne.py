# -*- coding: utf-8 -*-
"""
Spyder Editor

Created by Alejandro Gutierrez
"""

import Tkinter as tk
import tkMessageBox

"""window"""
window= tk.Tk();
window.title("First");
window.geometry('200x100');
window.resizable(width=False, height=False)
#window.configure(background='pale turquoise');

############### Attributes
txtNm1=tk.StringVar();
txtNm2=tk.StringVar();
txtResult=tk.StringVar();


###############
######################--Frame----##
vp=tk.Frame(window);
vp.grid(column=0,row=0,padx=(50,50),pady=(10,10)); #""""""
vp.columnconfigure(0,weight=1);
vp.rowconfigure(0,weight=1);
lblNumber1=tk.Label(vp,text="Number 1:", fg='black');
lblNumber2=tk.Label(vp,text="Number 2:", fg='black');
lblTotal=tk.Label(vp,text="Total:", fg='black');
btnSum=tk.Button(vp, text="Sum", fg='black', bg='gray',width=8,command= lambda:SumNumber());
txtNumber1=tk.Entry(vp,width=10,textvariable=txtNm1);
txtNumber2=tk.Entry(vp,width=10,textvariable=txtNm2);
txtTotal=tk.Entry(vp,width=10,textvariable=txtResult);
lblNumber1.grid(column=1,row=1,sticky="w");
lblNumber2.grid(column=1,row=2,sticky="w");
btnSum.grid(column=1,row=4,sticky="w");
txtNumber1.grid(column=2,row=1,sticky="w");
txtNumber2.grid(column=2,row=2,sticky="w");
lblTotal.grid(column=1,row=3,sticky="w");
txtTotal.grid(column=2,row=3,sticky="w");
txtTotal.config( state='disabled')
window.mainloop();
########----Methods--############
def SumNumber():

        if ((only_numbers((txtNm1.get()).strip()))and(only_numbers((txtNm2.get()).strip()))):
            txtResult.set( float( txtNm1.get() ) + float(txtNm2.get() ) );
        else :
          tkMessageBox.showinfo(message="Please insert numbers to sum, inside blanck spaces.", title="Info")
        
        
    

def only_numbers(numero):
    
    try:
        float(numero)
        return True;

    except ValueError: 
        return False;

             
        


