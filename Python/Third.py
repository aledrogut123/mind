

# -*- coding: utf-8 -*-
"""
Spyder Editor

Created by Alejandro Gutierrez
"""

import Tkinter as tk
import tkMessageBox

"""window"""
window= tk.Tk();
window.title("Third");
window.geometry('500x200');
window.resizable(width=False, height=False)
#window.configure(background='pale turquoise');

############### Attributes
txtNm1=tk.StringVar();
txtNm2=tk.StringVar();
txtListA=tk.StringVar();
txtListB=tk.StringVar();
txtResult=tk.StringVar();
ListToSumA=[];
ListToSumB=[];
ListToSumResult=[];


###############
######################--Frame----##
vp=tk.Frame(window);
vp.grid(column=0,row=0,padx=(50,50),pady=(10,10)); #""""""
vp.columnconfigure(0,weight=1);
vp.rowconfigure(0,weight=1);

lblNumber1=tk.Label(vp,text="Number A:", fg='black');
lblNumber2=tk.Label(vp,text="Number B:", fg='black');
lblShowListA=tk.Label(vp,text="List A:", fg='black');
lblShowListB=tk.Label(vp,text="List B:", fg='black');
lblTotal=tk.Label(vp,text="Total:", fg='black');

btnSum=tk.Button(vp, text="Sum", fg='black', bg='gray',width=8,command= lambda:SumNumber());
btnAdd=tk.Button(vp, text="Add to lists", fg='black', bg='gray',width=8,command= lambda:addList());
btnClear=tk.Button(vp, text="Clear all", fg='black', bg='gray',width=8,command= lambda:clear());

txtNumber1=tk.Entry(vp,width=10,textvariable=txtNm1);
txtNumber2=tk.Entry(vp,width=10,textvariable=txtNm2);
txtShowListA=tk.Entry(vp,width=50,textvariable=txtListA);
txtShowListB=tk.Entry(vp,width=50,textvariable=txtListB);
txtTotal=tk.Entry(vp,width=50,textvariable=txtResult);

lblNumber1.grid(column=1,row=1,sticky="w");
lblNumber2.grid(column=1,row=3,sticky="w");
lblShowListA.grid(column=1,row=2,sticky="w");
lblShowListB.grid(column=1,row=4,sticky="w");

btnSum.grid(column=1,row=6,sticky="w");
btnAdd.grid(column=0,row=6,sticky="w");
btnClear.grid(column=2,row=6,sticky="w");

txtNumber1.grid(column=2,row=1,sticky="w");
txtNumber2.grid(column=2,row=3,sticky="w");
txtShowListA.grid(column=2,row=2,sticky="w");
txtShowListA.config( state='disabled')
txtShowListB.grid(column=2,row=4,sticky="w");
txtShowListB.config( state='disabled')
lblTotal.grid(column=1,row=5,sticky="w");
txtTotal.grid(column=2,row=5,sticky="w");
txtTotal.config( state='disabled')
window.mainloop();
########----Methods--############
def SumNumber():
    for x in range(0,len(ListToSumA)):
        ListToSumResult.append(float(ListToSumA[x])+float(ListToSumB[x]));
 
    txtResult.set(ListToSumResult);
     
  
def clear():
    txtNm1.set('');
    txtNm2.set('');
    txtListA.set('');
    txtListB.set('');
    txtResult.set('');
    del ListToSumA[:];
    del ListToSumB[:];
    del ListToSumResult[:];
    
      

def only_numbers(numbersA,numbersB ):
    
    try:
        float(numbersB)
        float(numbersA)
        return True;

    except ValueError: 
        return False;
    
def addList():
    
        if (only_numbers((txtNm1.get()),(txtNm2.get()))):
            ListToSumB.append( float(txtNm2.get()));            
            ListToSumA.append( float(txtNm1.get()));
            txtListA.set( str(ListToSumA));
            txtListB.set( str(ListToSumB));
            txtNm1.set('')
            txtNm2.set('')
        else :
          tkMessageBox.showinfo(message="Please insert numbers to sum, inside blanck spaces.", title="Info")