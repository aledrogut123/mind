

# -*- coding: utf-8 -*-
"""
Spyder Editor

Created by Alejandro Gutierrez
"""

import Tkinter as tk
import tkMessageBox

"""window"""
window= tk.Tk();
window.title("Second");
window.geometry('500x100');
window.resizable(width=False, height=False)
#window.configure(background='pale turquoise');

############### Attributes
txtNm1=tk.StringVar();
txtList=tk.StringVar();
txtResult=tk.StringVar();
ListToSum=[];


###############
######################--Frame----##
vp=tk.Frame(window);
vp.grid(column=0,row=0,padx=(50,50),pady=(10,10)); #""""""
vp.columnconfigure(0,weight=1);
vp.rowconfigure(0,weight=1);

lblNumber1=tk.Label(vp,text="Number 1:", fg='black');
lblShowList=tk.Label(vp,text="List :", fg='black');
lblTotal=tk.Label(vp,text="Total:", fg='black');

btnSum=tk.Button(vp, text="Sum", fg='black', bg='gray',width=8,command= lambda:SumNumber());
btnAdd=tk.Button(vp, text="Add to list", fg='black', bg='gray',width=8,command= lambda:addList());
btnClear=tk.Button(vp, text="Clear all", fg='black', bg='gray',width=8,command= lambda:clear());

txtNumber1=tk.Entry(vp,width=10,textvariable=txtNm1);
txtShowList=tk.Entry(vp,width=50,textvariable=txtList);
txtTotal=tk.Entry(vp,width=10,textvariable=txtResult);

lblNumber1.grid(column=1,row=1,sticky="w");
lblShowList.grid(column=1,row=2,sticky="w");

btnSum.grid(column=1,row=4,sticky="w");
btnAdd.grid(column=0,row=4,sticky="w");
btnClear.grid(column=2,row=4,sticky="w");

txtNumber1.grid(column=2,row=1,sticky="w");
txtShowList.grid(column=2,row=2,sticky="w");
txtShowList.config( state='disabled')
lblTotal.grid(column=1,row=3,sticky="w");
txtTotal.grid(column=2,row=3,sticky="w");
txtTotal.config( state='disabled')
window.mainloop();
########----Methods--############
def SumNumber():
    txtResult.set( sum( ListToSum ) );
     
  
def clear():
    txtNm1.set('');
    txtList.set('');
    txtResult.set('');
    del ListToSum[:];
    
      

def only_numbers(numbers):
    
    try:
        float(numbers)
        return True;

    except ValueError: 
        return False;
    
def addList():
    
        if ((only_numbers((txtNm1.get()).strip()))):
            ListToSum.append( float(txtNm1.get()));
            txtList.set( str(ListToSum));
            txtNm1.set('')
        else :
          tkMessageBox.showinfo(message="Please insert numbers to sum, inside blanck spaces.", title="Info")
    
    
    
    
