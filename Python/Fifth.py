


# -*- coding: utf-8 -*-
"""
Spyder Editor

Created by Alejandro Gutierrez
"""

import Tkinter as tk
import tkMessageBox
import ScrolledText

"""window"""
window= tk.Tk();
window.title("Fifth");
window.geometry('500x200');
window.resizable(width=False, height=False)
#window.configure(background='pale turquoise');

############### Attributes
txtNm1=tk.StringVar();
txtNm2=tk.StringVar();
txtNm3=tk.StringVar();
txtListA=tk.StringVar();
txtListB=tk.StringVar();
txtListC=tk.StringVar();
txtResult=tk.StringVar();
ListToSumA=[];
ListToSumB=[];
ListToSumC=[];
ListToSumResult=[];


###############
######################--Frame----##
vp=tk.Frame(window);
vp.grid(column=0,row=0,padx=(50,50),pady=(10,10)); #""""""
vp.columnconfigure(0,weight=1);
vp.rowconfigure(0,weight=1);

lblNumber1=tk.Label(vp,text="Number A:", fg='black');
lblNumber2=tk.Label(vp,text="Number B:", fg='black');
lblNumber3=tk.Label(vp,text="Number C:", fg='black');
lblShowListA=tk.Label(vp,text="List A:", fg='black');
lblShowListB=tk.Label(vp,text="List B:", fg='black');
lblShowListC=tk.Label(vp,text="List C:", fg='black');
lblTotal=tk.Label(vp,text="Total:", fg='black');

btnSum=tk.Button(vp, text="Compare", fg='black', bg='gray',width=8,command= lambda:CompareNumbers());
btnAdd=tk.Button(vp, text="Add to lists", fg='black', bg='gray',width=8,command= lambda:addList());
btnClear=tk.Button(vp, text="Clear all", fg='black', bg='gray',width=8,command= lambda:clear());

txtNumber1=tk.Entry(vp,width=10,textvariable=txtNm1);
txtNumber2=tk.Entry(vp,width=10,textvariable=txtNm2);
txtNumber3=ScrolledText(vp,width=10,textvariable=txtNm3);
txtShowListA=tk.Entry(vp,width=50,textvariable=txtListA);
txtShowListB=tk.Entry(vp,width=50,textvariable=txtListB);
txtShowListC=tk.Entry(vp,width=50,textvariable=txtListC);
txtTotal=tk.Entry(vp,width=50,textvariable=txtResult);

lblNumber1.grid(column=1,row=1,sticky="w");
lblNumber2.grid(column=1,row=3,sticky="w");
lblNumber3.grid(column=1,row=5,sticky="w");
lblShowListA.grid(column=1,row=2,sticky="w");
lblShowListB.grid(column=1,row=4,sticky="w");
lblShowListC.grid(column=1,row=6,sticky="w");

btnSum.grid(column=1,row=8,sticky="w");
btnAdd.grid(column=0,row=8,sticky="w");
btnClear.grid(column=2,row=8,sticky="w");

txtNumber1.grid(column=2,row=1,sticky="w");
txtNumber2.grid(column=2,row=3,sticky="w");
txtNumber3.grid(column=2,row=5,sticky="w");
txtShowListA.grid(column=2,row=2,sticky="w");
txtShowListA.config( state='disabled')
txtShowListB.grid(column=2,row=4,sticky="w");
txtShowListB.config( state='disabled')
txtShowListC.grid(column=2,row=6,sticky="w");
txtShowListC.config( state='disabled')
lblTotal.grid(column=1,row=7,sticky="w");
txtTotal.grid(column=2,row=7,sticky="w");
txtTotal.config( state='disabled')
window.mainloop();
########----Methods--############
def CompareNumbers():
    for x in range(0,len(ListToSumA)):
        
        if((float(ListToSumA[x])<=float(ListToSumB[x]))and(float(ListToSumA[x])<=float(ListToSumC[x]))):
            ListToSumResult.append(float(ListToSumA[x]));
        elif((float(ListToSumB[x])<=float(ListToSumA[x]))and(float(ListToSumB[x])<=float(ListToSumC[x]))):
            ListToSumResult.append(float(ListToSumB[x]));
        else:
            ListToSumResult.append(float(ListToSumB[x]));
        
            
    txtResult.set(ListToSumResult);
     
  
def clear():
    txtNm1.set('');
    txtNm2.set('');
    txtNm3.set('');
    txtListA.set('');
    txtListB.set('');
    txtListC.set('');
    txtResult.set('');
    del ListToSumA[:];
    del ListToSumB[:];
    del ListToSumC[:];
    del ListToSumResult[:];
    
      

def only_numbers(numbersA,numbersB,numbersC ):
    
    try:
        float(numbersC)
        float(numbersB)
        float(numbersA)
        return True;

    except ValueError: 
        return False;
    
def addList():
    
        if (only_numbers((txtNm1.get()),(txtNm2.get()),((txtNm3.get())))):
            ListToSumB.append( float(txtNm2.get()));            
            ListToSumA.append( float(txtNm1.get()));            
            ListToSumC.append( float(txtNm3.get()));
            txtListA.set( str(ListToSumA));
            txtListB.set( str(ListToSumB));
            txtListC.set( str(ListToSumC));
            txtNm1.set('')
            txtNm2.set('')
            txtNm3.set('')
        else :
          tkMessageBox.showinfo(message="Please insert numbers to sum, inside blanck spaces.", title="Info")